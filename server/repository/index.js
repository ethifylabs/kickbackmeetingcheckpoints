const IdentityWallet = require("identity-wallet");
const Box = require("3box");
const { request } = require("graphql-request");
const crypto = require("crypto");
const moment = require("moment");
// const { v1: uuidv1 } = require("uuid");

class Repository {
  constructor() {
    this.configure3Box();
  }
  async getConsent({ type, origin, spaces }) {
    return true;
  }

  async configure3Box() {
    const seed =
      "0x4f3edf983ac636a65a842ce7c78d9aa706d3b113bce9c46f30d7d21715b23b1d";

    const idWallet = new IdentityWallet(this.getConsent, { seed });
    console.info("Id wallet done...");
    const threeIdProvider = idWallet.get3idProvider();
    console.info("three Id provider done...");
    const box = await Box.openBox(null, threeIdProvider);
    console.info("Start syncing up box...");
    await box.syncDone;
    console.info("Done setting up box...");
    this.space = await box.openSpace("kickback");
    console.info("Done setting up space...");
  }

  async queryKickbackEvent(contractAddress) {
    const query = `{
                    party(address: "${contractAddress}") {
                      address
                      name
                      start
                      end
                      headerImg
                      balance
                      deposit
                      roles {
                        role
                        user {
                          address
                          username
                        }
                      }
                      participants {
                        user {
                          address
                          username
                        }
                      }
                    }
                  }`;

    const resp = await request(
      "https://live.api.kickback.events/graphql",
      query
    );

    return resp;
  }

  async getAllEvent(userId, res) {
    try {
      // const space = await this.configure3Box();
      const events = await this.space.public.get(userId);

      res.json({ message: "Successfully retrieved all events", events });
    } catch (err) {
      console.log(err);
    }
  }

  async getEvent(userId, eventId, res) {
    try {
      // const resp = await this.queryKickbackEvent(eventId);
      // const space = await this.configure3Box();
      const data = await this.space.public.get(userId);
      const event = data.filter(
        (event) => event.id.toLowerCase() === eventId.toLowerCase()
      )[0];

      res.json({ message: "Successfully retrieved the event", event });
    } catch (err) {
      console.log(err);
    }
  }

  async addEvent(userId, eventData, res) {
    try {
      // TODO: Query & Add event to 3Box
      const eventDetails = await this.queryKickbackEvent(
        eventData.contractAddress
      );
      const id = crypto.randomBytes(16).toString("hex");
      const event = {
        id,
        ...eventDetails.party,
        minCheckpoint: Number.parseInt(eventData.minCheckpoint),
        checkpoints: [],
      };
      // const space = await this.configure3Box();
      const events = await this.space.public.get(userId);
      if (!events) {
        await this.space.public.set(userId, [event]);
      } else {
        if (events.length === 0) {
          await this.space.public.set(userId, [event]);
        } else {
          await this.space.public.set(userId, [...events, event]);
        }
      }

      console.log(event);

      res.json({ message: "Successfully added event", event });
    } catch (err) {
      console.log(err);
    }
  }

  async updateEvent(userId, eventId, eventData, res) {
    res.json({ message: "", data: {} });
  }

  async addCheckpoint(userId, eventId, checkpointData, res) {
    try {
      // TODO: Query & Add event to 3Box
      const id = crypto.randomBytes(16).toString("hex");
      const checkpoint = {
        id,
        name: checkpointData.name,
        minCheckpoint: Number.parseInt(checkpointData.minCheckpoint),
        status: "Not Started",
        quizzes: [],
      };
      const events = await this.space.public.get(userId);
      const addCheckpointToEvent = (pEvents) => {
        return new Promise((resolve, reject) => {
          pEvents.forEach((event) => {
            if (event.id === eventId) {
              event.checkpoints.push(checkpoint);
              resolve(pEvents);
            }
          });
        });
      };
      const resEvent = await addCheckpointToEvent(events);
      await this.space.public.set(userId, resEvent);
      res.json({ message: "Successfully added checkpoint", checkpoint });
    } catch (err) {
      console.log(err);
    }
  }

  async updateCheckpoint(userId, eventId, checkpointId, checkpointData, res) {
    res.json({ message: "", data: {} });
  }

  async deleteCheckpoint(userId, eventId, checkpointId, res) {
    try {
      // TODO: Query & Add event to 3Box
      const events = await this.space.public.get(userId);
      const deleteCheckpointToEvent = (pEvents) => {
        return new Promise((resolve, reject) => {
          pEvents.forEach((event, i) => {
            if (event.id === eventId) {
              const resCheckpoints = event.checkpoints.filter(
                (checkpoint) => checkpoint.id !== checkpointId
              );
              pEvents[i].checkpoints = resCheckpoints;
              resolve(pEvents);
            }
          });
        });
      };
      const resEvent = await deleteCheckpointToEvent(events);
      await this.space.public.set(userId, resEvent);
      res.json({ message: "Checkpoint deleted Successfully" });
    } catch (err) {
      console.log(err);
    }
  }

  async getAllQuizOfCheckpoint(userId, eventId, checkpointId, res) {
    try {
      // TODO: Query & Add event to 3Box
      const events = await this.space.public.get(userId);
      const selEvent = events.filter(
        (event) => event.id.toLowerCase() === eventId.toLowerCase()
      )[0];
      const selCheckpoint = selEvent.checkpoints.filter(
        (checkpoint) =>
          checkpoint.id.toLowerCase() === checkpointId.toLowerCase()
      )[0];

      res.json({
        message: "Successfully retrieved checkpoint",
        checkpoint: selCheckpoint,
      });
    } catch (err) {
      console.log(err);
    }
  }

  async getQuiz(userId, eventId, checkpointId, quizId, res) {
    try {
      // TODO: Query & Add event to 3Box
      const events = await this.space.public.get(userId);
      const selEvent = events.filter(
        (event) => event.id.toLowerCase() === eventId.toLowerCase()
      )[0];
      const selCheckpoint = selEvent.checkpoints.filter(
        (checkpoint) =>
          checkpoint.id.toLowerCase() === checkpointId.toLowerCase()
      )[0];
      const selQuiz = selCheckpoint.quizzes.filter(
        (quiz) => quiz.id.toLowerCase() === quizId.toLowerCase()
      )[0];
      const quiz = {
        ...selQuiz,
        answer: null,
        submissions: null,
      };
      res.json({
        message: "Successfully retrieved quiz",
        quiz,
      });
    } catch (err) {
      console.log(err);
    }
  }

  async addQuiz(userId, eventId, checkpointId, quizData, res) {
    try {
      // TODO: Query & Add event to 3Box
      const id = crypto.randomBytes(16).toString("hex");
      const quiz = {
        id,
        ...quizData,
        startedTimestamp: null,
        endTimestamp: null,
        status: "Not Started",
        submissions: [],
      };
      const events = await this.space.public.get(userId);
      const addQuizzesToEvent = (pEvents) => {
        return new Promise((resolve, reject) => {
          pEvents.forEach((event, i1) => {
            if (event.id === eventId) {
              event.checkpoints.forEach((checkpoint, i2) => {
                if (checkpoint.id === checkpointId) {
                  pEvents[i1].checkpoints[i2].quizzes.push(quiz);
                  resolve(pEvents);
                }
              });
            }
          });
        });
      };
      const resEvent = await addQuizzesToEvent(events);
      await this.space.public.set(userId, resEvent);
      res.json({ message: "Successfully added quizzes", quiz });
    } catch (err) {
      console.log(err);
    }
  }

  async updateQuiz(userId, eventId, checkpointId, quizId, quizData, res) {
    try {
      const events = await this.space.public.get(userId);
      const updateQuizzesToEvent = (pEvents) => {
        return new Promise((resolve, reject) => {
          pEvents.forEach((event, i1) => {
            if (event.id === eventId) {
              event.checkpoints.forEach((checkpoint, i2) => {
                if (checkpoint.id === checkpointId) {
                  checkpoint.quizzes.forEach((quiz, i3) => {
                    if (quiz.id === quizId) {
                      let resQuiz = quiz;
                      if (quizData.type.toLowerCase() === "start quiz") {
                        resQuiz = {
                          ...quiz,
                          startedTimestamp: moment().format(),
                          status: "Started",
                        };
                      } else if (quizData.type.toLowerCase() === "end quiz") {
                        resQuiz = {
                          ...quiz,
                          endTimestamp: moment().format(),
                          status: "Done",
                        };
                      } else if (quizData.type.toLowerCase() === "submission") {
                        const isSubmitted =
                          quiz.submissions.filter(
                            (submission) =>
                              submission.participantAddress.toLowerCase() ===
                              quizData.submissionData.participantAddress.toLowerCase()
                          ).length > 0;
                        if (!isSubmitted) {
                          const submission = {
                            ...quizData.submissionData,
                            isCorrect:
                              quizData.submissionData.answer === quiz.answer,
                            submissionTimestamp: moment().format(),
                          };
                          resQuiz = {
                            ...quiz,
                            submissions: [...quiz.submissions, submission],
                          };
                        }
                      }
                      pEvents[i1].checkpoints[i2].quizzes[i3] = resQuiz;
                      resolve(pEvents);
                    }
                  });
                }
              });
            }
          });
        });
      };
      const resEvent = await updateQuizzesToEvent(events);
      await this.space.public.set(userId, resEvent);
      res.json({ message: "Quiz updated Successfully" });
    } catch (err) {
      console.log(err);
    }
  }

  async deleteQuiz(userId, eventId, checkpointId, quizId, res) {
    try {
      const events = await this.space.public.get(userId);
      const deleteQuizzesToEvent = (pEvents) => {
        return new Promise((resolve, reject) => {
          pEvents.forEach((event, i1) => {
            if (event.id === eventId) {
              event.checkpoints.forEach((checkpoint, i2) => {
                if (checkpoint.id === checkpointId) {
                  const resQuizzes = checkpoint.quizzes.filter(
                    (quiz) => quiz.id !== quizId
                  );
                  pEvents[i1].checkpoints[i2].quizzes = resQuizzes;
                  resolve(pEvents);
                }
              });
            }
          });
        });
      };
      const resEvent = await deleteQuizzesToEvent(events);
      await this.space.public.set(userId, resEvent);
      res.json({ message: "Quiz deleted Successfully" });
    } catch (err) {
      console.log(err);
    }
  }

  async checkinEventParticipants(userId, eventId, res) {
    try {
      const data = await this.space.public.get(userId);
      const event = data.filter(
        (event) => event.id.toLowerCase() === eventId.toLowerCase()
      )[0];
      const checkedinParticipants = [];
      event.participants.forEach((participant) => {
        let checkpointCleared = 0;
        event.checkpoints.forEach((checkpoint) => {
          let quizzesCleared = 0;
          checkpoint.quizzes.forEach((quiz) => {
            quiz.submissions.forEach((submission) => {
              if (
                submission.participantAddress.toLowerCase() ===
                  participant.user.address.toLowerCase() &&
                submission.isCorrect
              ) {
                quizzesCleared++;
              }
            });
          });
          if (quizzesCleared >= checkpoint.minCheckpoint) {
            checkpointCleared++;
          }
        });
        if (checkpointCleared >= event.minCheckpoint) {
          checkedinParticipants.push(participant);
        }
      });

      res.json({
        message: "Successfully retrieved the checkin participants",
        participants: checkedinParticipants,
      });
    } catch (err) {
      console.log(err);
    }
  }
}
module.exports = new Repository();
