const repository = require("../repository");
module.exports = (app) => {
  app.get("/user/:userId/events", (req, res) => {
    repository.getAllEvent(req.params.userId, res);
  });

  app.get("/user/:userId/events/:id", (req, res) => {
    repository.getEvent(req.params.userId, req.params.id, res);
  });

  app.post("/user/:userId/events", (req, res) => {
    repository.addEvent(req.params.userId, req.body, res);
  });

  app.put("/user/:userId/events/:id", (req, res) => {
    repository.updateEvent(req.params.userId, req.params.id, req.body, res);
  });

  app.get("/organizer/:userId/events/:id/checkin", (req, res) => {
    repository.checkinEventParticipants(req.params.userId, req.params.id, res);
  });

  app.post("/user/:userId/events/:eventId/checkpoints", (req, res) => {
    repository.addCheckpoint(
      req.params.userId,
      req.params.eventId,
      req.body,
      res
    );
  });

  app.put(
    "/user/:userId/events/:eventId/checkpoints/:checkpointId",
    (req, res) => {
      repository.updateCheckpoint(
        req.params.userId,
        req.params.eventId,
        req.params.checkpointId,
        req.body,
        res
      );
    }
  );

  app.delete(
    "/user/:userId/events/:eventId/checkpoints/:checkpointId",
    (req, res) => {
      repository.deleteCheckpoint(
        req.params.userId,
        req.params.eventId,
        req.params.checkpointId,
        res
      );
    }
  );

  app.get(
    "/user/:userId/events/:eventId/checkpoints/:checkpointId/quizzes",
    (req, res) => {
      repository.getAllQuizOfCheckpoint(
        req.params.userId,
        req.params.eventId,
        req.params.checkpointId,
        res
      );
    }
  );

  app.get(
    "/user/:userId/events/:eventId/checkpoints/:checkpointId/quizzes/:quizId",
    (req, res) => {
      repository.getQuiz(
        req.params.userId,
        req.params.eventId,
        req.params.checkpointId,
        req.params.quizId,
        res
      );
    }
  );

  app.post(
    "/user/:userId/events/:eventId/checkpoints/:checkpointId/quizzes",
    (req, res) => {
      repository.addQuiz(
        req.params.userId,
        req.params.eventId,
        req.params.checkpointId,
        req.body,
        res
      );
    }
  );

  app.put(
    "/user/:userId/events/:eventId/checkpoints/:checkpointId/quizzes/:quizId",
    (req, res) => {
      repository.updateQuiz(
        req.params.userId,
        req.params.eventId,
        req.params.checkpointId,
        req.params.quizId,
        req.body,
        res
      );
    }
  );

  app.delete(
    "/user/:userId/events/:eventId/checkpoints/:checkpointId/quizzes/:quizId",
    (req, res) => {
      repository.deleteQuiz(
        req.params.userId,
        req.params.eventId,
        req.params.checkpointId,
        req.params.quizId,
        res
      );
    }
  );
};
