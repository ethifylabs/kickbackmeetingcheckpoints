### Background


# Name of Project
Kickback Meeting Checkpoints

## Project Description

Due to COVID-19, people now engage more in online meetups but this also increases the possibility of not attending the online meetup. This increases the overhead for the organizers to check the progress of their participants and also the organizers need a process to automate the check-in in their meetup.

### Features

- Our proposal is an app where the organiser can create checkpoints for their online events in which the organisers can create quizzes based on the context of their event with a timer attached to it and then share it with the participants for them to solve. 
- The organizer has the power to start and end the quizzes. 
- To automate the process of check-in, the organizers can directly do it in the kickback website with just a click of a button that calculates the list of participants who have cleared all the checkpoints they created in their event based on the result of the quizzes participants have solved. 
- This will force the participants to take the checkpoints seriously otherwise they will lose their staked amount.
- We have used 3Box storage api for Database in node js server.

## Project Team
    
```
Mitrasish Mukherjee
@rekpero
```
```
Manank Patni
@manankpatni
```

## A prototype (code or no-code)
[https://amazing-thompson-a45502.netlify.app/](https://amazing-thompson-a45502.netlify.app/)
## Github Repo
https://gitlab.com/ethifylabs/kickbackmeetingcheckpoints
## Video Demo
https://youtu.be/ACL4niwJ84E


This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br />
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

### Analyzing the Bundle Size

This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

### Making a Progressive Web App

This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

### Advanced Configuration

This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

### Deployment

This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

### `yarn build` fails to minify

This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify
