import React from "react";
import "./App.scss";
import Header from "./components/Header";
import { withRouter, Route, Redirect } from "react-router";
import Modal from "./components/Modal";
import HomeScreen from "./components/HomeScreen";
import EventsScreen from "./components/EventsScreen";
import RiddlesScreen from "./components/RiddlesScreen";
import EventSingleScreen from "./components/EventSingleScreen";
import UserRiddleScreen from "./components/UserRiddleScreen";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      address: undefined,
      page: "home",
      openModal: false,
      modalConfig: {},
    };
  }
  changeAddress = (address) => {
    this.setState({ address });
  };

  changePage = (page) => {
    this.setState({ page });
  };

  setOpenModal = (pFlag) => {
    console.log("open modal", pFlag);
    this.setState({ openModal: pFlag });
  };

  setModalConfig = (pConfig) => {
    console.log("set modal config", pConfig);
    this.setState({ modalConfig: pConfig });
  };

  render() {
    return (
      <div className="App">
        <Modal
          setOpenModal={this.setOpenModal}
          openModal={this.state.openModal}
          config={this.state.modalConfig}
          address={this.state.address}
          setAddress={this.changeAddress}
        />
        <Header setAddress={this.changeAddress} address={this.state.address} />
        <Route
          path="/home"
          exact
          render={() => (
            <HomeScreen
              setAddress={this.changeAddress}
              address={this.state.address}
            />
          )}
        />

        <Route
          path="/events"
          exact
          render={() =>
            this.state.address ? (
              <EventsScreen
                setModalConfig={this.setModalConfig}
                setOpenModal={this.setOpenModal}
                address={this.state.address}
              />
            ) : (
                <Redirect to="/home" />
              )
          }
        />

        <Route
          path="/events/:eventId"
          exact
          render={() =>
            this.state.address ? (
              <EventSingleScreen
                setModalConfig={this.setModalConfig}
                setOpenModal={this.setOpenModal}
                address={this.state.address}
              />
            ) : (
                <Redirect to="/home" />
              )
          }
        />

        <Route
          path="/events/:eventId/checkpoint/:checkpointId"
          exact
          render={() =>
            this.state.address ? (
              <RiddlesScreen
                address={this.state.address}
                addMode={false}
                setModalConfig={this.setModalConfig}
                setOpenModal={this.setOpenModal} />
            ) : (
                <Redirect to="/home" />
              )
          }
        />

        <Route
          path="/events/:eventId/checkpoint/:checkpointId/riddles/add"
          exact
          render={() =>
            this.state.address ? (
              <RiddlesScreen address={this.state.address} addMode={true} />
            ) : (
                <Redirect to="/home" />
              )
          }
        />

        <Route
          path="/organiser/:organiserId/events/:eventId/checkpoint/:checkpointId/quiz/:quizId/exam"
          exact
          render={() => (
            <UserRiddleScreen
              address={this.state.address}
              setAddress={this.changeAddress}
            />
          )}
        />

        <Route exact path="/" render={() => <Redirect to="/home" />} />
      </div>
    );
  }
}

export default withRouter(App);
