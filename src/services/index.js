import Onboard from "bnc-onboard";
import * as Web3 from "web3";

let web3;

const onboard = Onboard({
  dappId: "052b3fe9-87d5-4614-b2e9-6dd81115979a", // [String] The API key created by step one above
  networkId: 1, // [Integer] The Ethereum network ID your Dapp uses.
  subscriptions: {
    wallet: (wallet) => {
      web3 = new Web3(wallet.provider);
    },
  },
});

export const getAccount = async () => {
  await onboard.walletSelect();
  await onboard.walletCheck();
  const currentState = onboard.getState();

  return currentState.address;
};

const API_URL = "http://meety-backend.herokuapp.com/";

export const getAllEvents = (userAddress) => {
  console.log(userAddress);
  return fetch(`${API_URL}/user/${userAddress}/events`).then((res) =>
    res.json()
  );
};

export const getEvent = (userAddress, eventId) => {
  console.log(userAddress, eventId);
  return fetch(`${API_URL}/user/${userAddress}/events/${eventId}`).then((res) =>
    res.json()
  );
};

export const addEvent = (eventContractAddress, minCheckpoint, userAddress) => {
  console.log(eventContractAddress, minCheckpoint, userAddress);
  const eventData = { contractAddress: eventContractAddress, minCheckpoint };
  return fetch(`${API_URL}/user/${userAddress}/events`, {
    headers: { "Content-Type": "application/json; charset=utf-8" },
    method: "POST",
    body: JSON.stringify(eventData),
  }).then((res) => res.json());
};

export const addCheckpoint = (
  userAddress,
  eventId,
  checkpointName,
  minCheckpoint
) => {
  console.log(userAddress, eventId, checkpointName, minCheckpoint);
  const checkpointData = { name: checkpointName, minCheckpoint };
  return fetch(`${API_URL}/user/${userAddress}/events/${eventId}/checkpoints`, {
    headers: { "Content-Type": "application/json; charset=utf-8" },
    method: "POST",
    body: JSON.stringify(checkpointData),
  }).then((res) => res.json());
};

export const deleteCheckpoint = (userAddress, eventId, checkpointId) => {
  console.log(userAddress, eventId, checkpointId);
  return fetch(
    `${API_URL}/user/${userAddress}/events/${eventId}/checkpoints/${checkpointId}`,
    {
      method: "DELETE",
    }
  ).then((res) => res.json());
};

export const getAllQuizzes = (userAddress, eventId, checkpointId) => {
  console.log(userAddress, eventId, checkpointId);
  return fetch(
    `${API_URL}/user/${userAddress}/events/${eventId}/checkpoints/${checkpointId}/quizzes`
  ).then((res) => res.json());
};

export const getQuiz = (organiserAddress, eventId, checkpointId, quizId) => {
  console.log(organiserAddress, eventId, checkpointId, quizId);
  return fetch(
    `${API_URL}/user/${organiserAddress}/events/${eventId}/checkpoints/${checkpointId}/quizzes/${quizId}`
  ).then((res) => res.json());
};

export const addQuiz = (userAddress, eventId, checkpointId, quizData) => {
  console.log(userAddress, eventId, checkpointId, quizData);
  return fetch(
    `${API_URL}/user/${userAddress}/events/${eventId}/checkpoints/${checkpointId}/quizzes`,
    {
      headers: { "Content-Type": "application/json; charset=utf-8" },
      method: "POST",
      body: JSON.stringify(quizData),
    }
  ).then((res) => res.json());
};

export const updateQuiz = (
  userAddress,
  eventId,
  checkpointId,
  quizId,
  quizData
) => {
  console.log(userAddress, eventId, checkpointId, quizData);
  return fetch(
    `${API_URL}/user/${userAddress}/events/${eventId}/checkpoints/${checkpointId}/quizzes/${quizId}`,
    {
      headers: { "Content-Type": "application/json; charset=utf-8" },
      method: "PUT",
      body: JSON.stringify(quizData),
    }
  ).then((res) => res.json());
};
