import React from "react";
import "./user_riddle_screen.scss";
import { getAccount } from "../../services";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight, faSyncAlt } from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-regular-svg-icons";
import { useParams } from "react-router";
import { getQuiz, updateQuiz } from "../../services";
import Loader from "../Loader";
import Timer from "../Timer";

function UserRiddleScreen(props) {
  const { organiserId, eventId, checkpointId, quizId } = useParams();

  const [riddle, setRiddle] = React.useState();
  const [answer, setAnswer] = React.useState(0);
  const [loading, setLoading] = React.useState(false);
  const [isSubmitted, setIsSubmitted] = React.useState(false);

  React.useEffect(() => {
    console.log(organiserId, eventId, checkpointId, quizId)
    handleGetQuiz()
  }, [])

  const handleGetQuiz = async () => {
    const resp = await getQuiz(organiserId, eventId, checkpointId, quizId);
    console.log(resp)
    setRiddle(resp.quiz);
  }


  const handleSetAnswer = (i) => {
    console.log(i);
    setAnswer(i);
  };

  const handleConnect = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const address = await getAccount();
        console.log(address);
        props.setAddress(address);
        resolve();
      } catch (err) {
        reject(err);
      }
    });
  };

  const connectWallet = async () => {
    try {
      await handleConnect();
    } catch (err) {
      console.log(err);
    }
  };

  const handleRefresh = async () => {
    setLoading(true);
    handleGetQuiz()
    setLoading(false);
  };

  const handleSubmitRiddle = async () => {
    setLoading(true);
    const quizData = { type: "submission", submissionData: { participantAddress: props.address, answer: riddle.options[answer] } }
    const resp = await updateQuiz(
      organiserId, eventId, checkpointId, quizId, quizData
    );
    console.log(resp)
    handleGetQuiz()
    setIsSubmitted(true)
    setLoading(false);
  };

  const timerEnd = () => {
    handleSubmitRiddle()
  }

  return (
    <div className="user-riddle-screen">
      <div>
        <div className="user-riddle-container">
          {loading ? (
            <Loader loaderType="circle" />
          ) : (
              !props.address ? <div>
                <div className="user-riddle-info">
                  {"Enable the wallet to access your question and wait for the organizer to start the riddle"}
                </div>
                {
                  <div className="metamask-enable">
                    <div
                      className="wallet-connect-container"
                      onClick={connectWallet}
                    >
                      <div className="address-container">
                        <img
                          src={require("../../assets/icons/metamask.svg")}
                          alt="metamask icon"
                          className="metamask-icon"
                        />
                        <span>Connect to Wallet</span>
                      </div>
                    </div>
                  </div>
                }
              </div> :
                riddle && <div>
                  {riddle.status === "Started" && !isSubmitted && (
                    <div>
                      <div className="user-riddle-header">
                        <div className="user-riddle-question">
                          {riddle.question}
                        </div>
                        <div className="user-riddle-status">
                          <span className="riddle-list-item-timer-icon">
                            <FontAwesomeIcon icon={faClock} />
                          </span>
                          <Timer timer={riddle.timer} status={riddle.status} timerEnd={timerEnd} />
                        </div>
                      </div>
                      <div className="user-riddle-options-container">
                        {riddle.options.map((option, i) => (
                          <div
                            className="user-riddle-option"
                            key={i}
                            onClick={(e) => handleSetAnswer(i)}
                          >
                            <input
                              type="radio"
                              className="user-riddle-option-radio"
                              onChange={(e) => setAnswer(i)}
                              checked={answer === i}
                            />{" "}
                            <label>{option}</label>
                          </div>
                        ))}
                      </div>
                      <div className="user-riddle-submit-container">
                        <button
                          type="button"
                          className="submit-button"
                          onClick={handleSubmitRiddle}
                        >
                          <span className="button-text">Submit</span>
                          <span className="button-icon">
                            <FontAwesomeIcon icon={faAngleRight} />
                          </span>
                        </button>
                      </div>
                    </div>
                  )}
                  {riddle.status === "Not Started" && !isSubmitted && (
                    <div>
                      <div className="user-riddle-info">
                        {"Wait for the organizer to start the riddle and then click refresh button to get the riddle"}
                      </div>
                      <div className="refresh-button">
                        <button
                          type="button"
                          className="submit-button"
                          onClick={handleRefresh}
                        >
                          <span className="button-icon">
                            <FontAwesomeIcon icon={faSyncAlt} />
                          </span>
                          <span className="button-text">Refresh</span>
                        </button>
                      </div>
                    </div>
                  )}
                  {riddle.status === "Done" && !isSubmitted && (
                    <div>
                      <div className="done-gif-container">
                        <img
                          src="https://i.giphy.com/media/ehz3LfVj7NvpY8jYUY/source.gif"
                          alt="gif"
                          className="done-gif"
                        />
                      </div>
                      <div className="user-done-info">
                        The quiz is now closed. Please refer to event organiser to get further details.
                  </div>
                    </div>
                  )}
                  {isSubmitted && (
                    <div>
                      <div className="done-gif-container">
                        <img
                          src="https://i.giphy.com/media/ehz3LfVj7NvpY8jYUY/source.gif"
                          alt="gif"
                          className="done-gif"
                        />
                      </div>
                      <div className="user-done-info">
                        Thank you for your submission. You can close the window now.
                  </div>
                    </div>
                  )}
                </div>
            )}
        </div>
      </div>
    </div>
  );
}

export default UserRiddleScreen;
