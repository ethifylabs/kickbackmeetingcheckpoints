import React from "react";
import "./header.scss";
import { getAccount } from "../../services";
import makeBlockie from "ethereum-blockies-base64";
import { shortenAddress } from "../../utils";

function Header({ setAddress, address }) {
  const handleConnect = async () => {
    const address = await getAccount();
    setAddress(address);
  };
  const getAddressTemplate = (address) => {
    if (address) {
      return (
        <div className="address-container">
          <span>{shortenAddress(address)}</span>
          <img
            src={makeBlockie(address)}
            alt="address blockie"
            className="address-blockie"
          />
        </div>
      );
    } else {
      return (
        <div className="address-container">
          <img
            src={require("../../assets/icons/metamask.svg")}
            alt="metamask icon"
            className="metamask-icon"
          />
          <span>Connect to Wallet</span>
        </div>
      );
    }
  };
  return (
    <div className="header">
      <div className="header-first-container">
        <div className="logo-container">
          <img
            src="https://assets.bounties.network/production/userimages/0xffd1ac3e8818adcbe5c597ea076e8d3210b45df5-sm-01b2.png"
            alt="logo"
            className="app-logo"
          />
          <span className="logo-title">Meety</span>
        </div>
      </div>
      <div
        className={"wallet-container " + (address ? "connected" : null)}
        onClick={handleConnect}
      >
        {getAddressTemplate(address)}
      </div>
    </div>
  );
}

export default Header;
