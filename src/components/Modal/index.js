import React from "react";
import "./modal.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTimes, faAngleRight } from "@fortawesome/free-solid-svg-icons";
import {
  faCheckCircle,
  faTimesCircle,
  faCopy
} from "@fortawesome/free-regular-svg-icons";
import copy from "clipboard-copy";
import Loader from "../Loader";
import { addEvent, getAccount, addCheckpoint } from "../../services";

function Modal(props) {
  const [eventId, setEventId] = React.useState("");
  const [minCheckpoint, setMinCheckpoint] = React.useState("");
  const [checkpointName, setCheckpointName] = React.useState("");

  const handleAddEvent = async () => {
    console.log(eventId, minCheckpoint);
    try {
      let address;
      if (props.address) {
        address = props.address;
      } else {
        props.setOpenModal(false);
        address = await getAccount();
        props.setAddress(address);
        props.setOpenModal(true);
      }
      const resp = await addEvent(eventId, minCheckpoint, address);
      console.log("Add event resp", resp);
      props.config.setEvents([
        ...props.config.events,
        {
          ...resp.event,
          stakedAmount: "1.50 ETH"
            // Number.parseFloat(
            //   Number(resp.event.deposit) /
            //   Math.pow(10, Number.parseInt(resp.event.tokenDecimals))
            // ).toFixed(2) +
            // " " +
            // resp.event.tokenSymbol,
        },
      ]);
      setEventId("");
      setMinCheckpoint("");
      props.setOpenModal(false);
    } catch (err) {
      console.log(err);
    }
  };

  const handleAddCheckpoint = async () => {
    console.log(checkpointName);
    let address;
    if (props.address) {
      address = props.address;
    } else {
      props.setOpenModal(false);
      address = await getAccount();
      props.setAddress(address);
      props.setOpenModal(true);
    }
    const resp = await addCheckpoint(
      address,
      props.config.eventId,
      checkpointName,
      minCheckpoint
    );

    props.config.setCheckpoints([...props.config.checkpoints, resp.checkpoint]);
    setCheckpointName("");
    setMinCheckpoint("");
    props.setOpenModal(false);
  };

  const copyToClipboard = () => {
    copy(props.config.quizLink)
  }

  return (
    <div>
      <div
        className={`modal-overlay ${!props.openModal ? "closed" : null}`}
        id="modal-overlay"
        onClick={(e) => props.setOpenModal(false)}
      ></div>

      <div className={`modal ${!props.openModal ? "closed" : null}`} id="modal">
        <button
          className="close-button"
          id="close-button"
          onClick={(e) => props.setOpenModal(false)}
        >
          <FontAwesomeIcon icon={faTimes} />
        </button>
        <div className="modal-guts">
          {props.config.type === "transac" ? (
            <div className="modal-transac">
              <div className="modal-loader">
                {props.config.status === "pending" ? (
                  <Loader loaderType="circle" />
                ) : null}
                {props.config.status === "success" ? (
                  <div className="icon-container">
                    <FontAwesomeIcon
                      icon={faCheckCircle}
                      className="success-icon"
                    />
                  </div>
                ) : null}
                {props.config.status === "fail" ? (
                  <div className="icon-container">
                    <FontAwesomeIcon
                      icon={faTimesCircle}
                      className="fail-icon"
                    />
                  </div>
                ) : null}
              </div>
              <div
                className={`modal-title ${
                  props.config.status === "fail" ? "fail" : "success"
                  }`}
              >
                {props.config.title}
              </div>
              <div className="modal-message">{props.config.message}</div>
            </div>
          ) : null}
          {props.config.type === "add-event" ? (
            <div className="modal-add-event">
              <div className="modal-title">Add Event</div>
              <div className="form-elements">
                <label className="form-label">Kickback Event Id</label>
                <input
                  type="text"
                  className="form-input"
                  placeholder="Enter kickback event contract address"
                  value={eventId}
                  onChange={(e) => setEventId(e.target.value)}
                />
              </div>
              <div className="form-elements">
                <label className="form-label">Minimum Checkpoints</label>
                <input
                  type="number"
                  className="form-input"
                  placeholder="Enter min. checkpoint participants need to clear"
                  value={minCheckpoint}
                  onChange={(e) => setMinCheckpoint(e.target.value)}
                />
              </div>
              <div className="form-button-elements">
                <button
                  type="button"
                  className="submit-button"
                  onClick={handleAddEvent}
                >
                  <span className="button-text">Add Event</span>
                  <span className="button-icon">
                    <FontAwesomeIcon icon={faAngleRight} />
                  </span>
                </button>
              </div>
            </div>
          ) : null}
          {props.config.type === "add-checkpoint" ? (
            <div className="modal-add-checkpoint">
              <div className="modal-title">Add Checkpoint</div>
              <div className="form-elements">
                <label className="form-label">Checkpoint Name</label>
                <input
                  type="text"
                  className="form-input"
                  placeholder="Enter kickback event contract address"
                  value={checkpointName}
                  onChange={(e) => setCheckpointName(e.target.value)}
                />
              </div>
              <div className="form-elements">
                <label className="form-label">Minimum Quiz</label>
                <input
                  type="number"
                  className="form-input"
                  placeholder="Enter min. quiz participants need to clear"
                  value={minCheckpoint}
                  onChange={(e) => setMinCheckpoint(e.target.value)}
                />
              </div>
              <div className="form-button-elements">
                <button
                  type="button"
                  className="submit-button"
                  onClick={handleAddCheckpoint}
                >
                  <span className="button-text">Add Checkpoint</span>
                  <span className="button-icon">
                    <FontAwesomeIcon icon={faAngleRight} />
                  </span>
                </button>
              </div>
            </div>
          ) : null}
          {props.config.type === "show-quiz-link" ? (
            <div className="modal-show-quiz-link">
              <div className="modal-title">Your Quiz Link</div>
              <div className="form-elements">
                <div className="quiz-link-container" onClick={copyToClipboard}>
                  <span className="quiz-link">{props.config.quizLink}</span>
                  <span>
                    <FontAwesomeIcon icon={faCopy} />
                  </span>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </div>
    </div>
  );
}

export default Modal;
