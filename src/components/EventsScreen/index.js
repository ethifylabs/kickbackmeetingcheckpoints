import React from "react";
import "./events_screen.scss";
import { useHistory } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { getAllEvents } from "../../services";

function EventsScreen({ address, setOpenModal, setModalConfig }) {
  let history = useHistory();
  const [events, setEvents] = React.useState([]);

  React.useEffect(() => {
    handleGetAllEvents();
  }, []);

  const handleGetAllEvents = async () => {
    const resp = await getAllEvents(address);
    const gotEvents = resp.events;
    if (gotEvents) {
      const resEvents = gotEvents.map((event) => ({
        ...event,
        stakedAmount: "1.50 ETH"
          // Number.parseFloat(
          //   Number(event.deposit) /
          //     Math.pow(10, Number.parseInt(event.tokenDecimals))
          // ).toFixed(2) +
          // " " +
          // event.tokenSymbol,
      }));
      setEvents(resEvents);
    }
  };

  const openEvent = (evt) => {
    history.push("/events/" + evt.id);
  };

  const openAddEventModal = () => {
    console.log("Enter handle open modal");
    setModalConfig({ type: "add-event", events, setEvents });
    setOpenModal(true);
  };
  return (
    <div className="events-screen">
      <div>
        <div className="event-header">
          <h2>Events</h2>
          <button className="create-event-button" onClick={openAddEventModal}>
            <span className="button-icon">
              <FontAwesomeIcon icon={faPlus} />
            </span>
            <span className="button-text">Add Events</span>
          </button>
        </div>
        <ul className="event-list-container">
          {events &&
            events.length > 0 ? events.map((evt, i1) => (
              <li
                className="event-list-item"
                key={i1}
                onClick={(e) => openEvent(evt)}
              >
                <div className="event-list-item-container">
                  <div className="event-item-header">
                    {evt.headerImg && (
                      <img
                        src={evt.headerImg}
                        alt="event-img"
                        className="event-image"
                      />
                    )}
                  </div>
                  <div className="event-item-body-container">
                    <div className="event-name">
                      {evt.name ? evt.name : "Event"}
                    </div>
                    <div className="event-checkin">
                      <b>{evt.minCheckpoint}</b> Minimum check-in required
                    </div>
                    <div className="event-stake">
                      <span className="event-amount-staked">
                        {evt.stakedAmount}
                      </span>
                    </div>
                  </div>
                </div>
              </li>
            )) : "No Event Added..."}
        </ul>
      </div>
    </div>
  );
}

export default EventsScreen;
