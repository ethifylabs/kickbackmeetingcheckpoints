import React from "react";

class Timer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            minutes: 0,
            seconds: 0,
        }
    }

    componentDidMount() {
        const mins = Number.parseInt(this.props.timer.split(":")[0])
        const secs = Number.parseInt(this.props.timer.split(":")[1])
        this.setState({ minutes: mins, seconds: secs }, () => {
            if (this.props.status === "Started") {
                this.startTimer()
            }
        });


    }

    startTimer = () => {
        this.myInterval = setInterval(() => {
            const { seconds, minutes } = this.state

            if (seconds > 0) {
                this.setState(({ seconds }) => ({
                    seconds: seconds - 1
                }))
            }
            if (seconds === 0) {
                if (minutes === 0) {
                    clearInterval(this.myInterval)
                    this.props.timerEnd()
                } else {
                    this.setState(({ minutes }) => ({
                        minutes: minutes - 1,
                        seconds: 59
                    }))
                }
            }
        }, 1000)
    }

    componentWillUnmount() {
        clearInterval(this.myInterval)
    }

    render() {
        const { seconds, minutes } = this.state
        return (
            <span>{minutes < 10 ? "0" + minutes : minutes}:{seconds < 10 ? "0" + seconds : seconds}</span>
        )
    }
}

export default Timer;