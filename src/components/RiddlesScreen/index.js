import React from "react";
import "./riddles_screen.scss";
import { withRouter } from "react-router-dom";
import TimeField from "react-simple-timefield";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPlus,
  faTrashAlt,
  faAngleRight,
} from "@fortawesome/free-solid-svg-icons";
import { faClock } from "@fortawesome/free-regular-svg-icons";
import { getAllQuizzes, addQuiz, updateQuiz } from "../../services";

class RiddlesScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      question: "",
      currentOption: "",
      options: [],
      answer: "",
      timer: "05:00",
      riddlesList: [],
      checkpoint: null,
    };
  }

  async componentDidMount() {
    console.log(
      "Run componentDidMount",
      this.props.match.params.eventId,
      this.props.match.params.checkpointId
    );
    this.handleGetQuizzes();
  }

  handleGetQuizzes = async () => {
    const resp = await getAllQuizzes(
      this.props.address,
      this.props.match.params.eventId,
      this.props.match.params.checkpointId
    );
    this.setState({
      riddlesList: resp.checkpoint.quizzes,
      checkpoint: resp.checkpoint,
    });
  };

  addOptions = () => {
    if(this.state.currentOption) {
      this.setState({
        options: [...this.state.options, this.state.currentOption],
        currentOption: "",
      });
    }
  };

  removeOption = (pOption) => {
    const { options } = this.state;
    let index = options.indexOf(pOption);
    options.splice(index, 1);
    this.setState({ options });
  };

  submitRiddle = async () => {
    const { question, options, answer, timer } = this.state;
    const quizData = { question, options, answer, timer };
    const resp = await addQuiz(
      this.props.address,
      this.props.match.params.eventId,
      this.props.match.params.checkpointId,
      quizData
    );
    console.log(resp);
    this.props.history.push(
      `/events/${this.props.match.params.eventId}/checkpoint/${this.props.match.params.checkpointId}`
    );
  };

  toggleAnswer = (pQuestionIndex) => {
    const { riddlesList } = this.state;
    const riddle = riddlesList[pQuestionIndex];
    riddle.showAnswer = !riddle.showAnswer;
    riddlesList[pQuestionIndex] = riddle;
    this.setState({ riddlesList });
  };

  goBack = (pFlag) => {
    if (pFlag) {
      this.props.history.push(
        `/events/${this.props.match.params.eventId}/checkpoint/${this.props.match.params.checkpointId}`
      );
    } else {
      this.props.history.push(`/events/${this.props.match.params.eventId}`);
    }
  };

  startTimer = async (quiz) => {
    const quizData = { type: "start quiz" };
    const resp = await updateQuiz(
      this.props.address,
      this.props.match.params.eventId,
      this.props.match.params.checkpointId,
      quiz.id,
      quizData
    );
    console.log(resp);
    this.handleGetQuizzes();
  };

  stopTimer = async (quiz) => {
    const quizData = { type: "end quiz" };
    const resp = await updateQuiz(
      this.props.address,
      this.props.match.params.eventId,
      this.props.match.params.checkpointId,
      quiz.id,
      quizData
    );
    console.log(resp);
    this.handleGetQuizzes();
  };

  shareLink = (quiz) => {
    this.props.setModalConfig({
      type: "show-quiz-link",
      quizLink: `https://amazing-thompson-a45502.netlify.app/organiser/${this.props.address}/events/${this.props.match.params.eventId}/checkpoint/${this.props.match.params.checkpointId}/quiz/${quiz.id}/exam`,
    });
    this.props.setOpenModal(true);
  };

  render() {
    const {
      question,
      currentOption,
      options,
      answer,
      riddlesList,
      timer,
      checkpoint,
    } = this.state;
    return (
      <div className="riddles-screen">
        {!this.props.addMode ? (
          checkpoint && (
            <div>
              <div className="riddles-header">
                <div className="riddles-header-left">
                  <img
                    src={require("../../assets/icons/back.svg")}
                    alt="back"
                    className="riddles-back-button"
                    onClick={(e) => this.goBack(false)}
                  />
                  <div>
                    <h2>{checkpoint.name}</h2>
                    <span>
                      {checkpoint.minCheckpoint} Quizzes need to be cleared
                    </span>
                  </div>
                </div>
                <button
                  className="create-riddles-button"
                  onClick={(e) =>
                    this.props.history.push(
                      `/events/${this.props.match.params.eventId}/checkpoint/${this.props.match.params.checkpointId}/riddles/add`
                    )
                  }
                >
                  <span className="button-icon">
                    <FontAwesomeIcon icon={faPlus} />
                  </span>
                  <span className="button-text">Create Quizzes</span>
                </button>
              </div>
              <ul className="riddles-list-container">
                {riddlesList.length > 0
                  ? riddlesList.map((riddle, i1) => (
                      <li className="riddle-list-item" key={i1}>
                        <div className="riddle-list-item-container">
                          <div className="riddle-list-item-header">
                            <div className="riddle-list-item-question">
                              {riddle.question}
                            </div>
                            <div className="riddle-list-item-status">
                              <div className="riddle-list-item-status-text">
                                {riddle.status}
                              </div>
                              {riddle.timer ? (
                                <div className="timer-container">
                                  <span className="riddle-list-item-timer-icon">
                                    <FontAwesomeIcon icon={faClock} />
                                  </span>
                                  <span>{riddle.timer}</span>
                                </div>
                              ) : null}
                            </div>
                          </div>
                          <ol
                            className={`riddle-list-item-options ${
                              riddle.showAnswer ? "" : "margin-set"
                            }`}
                          >
                            {riddle.options.map((option, i2) => (
                              <li key={i2}>{option}</li>
                            ))}
                          </ol>
                          {riddle.showAnswer ? (
                            <div className="riddle-list-item-answer">
                              <span className="riddle-list-item-answer-title">
                                Answer:{" "}
                              </span>
                              <span>{riddle.answer}</span>
                            </div>
                          ) : null}
                          <div className="riddle-list-item-actions">
                            <button
                              className="riddle-list-item-action-button"
                              onClick={(e) => this.toggleAnswer(i1)}
                            >
                              {riddle.showAnswer ? "Hide" : "Reveal"} Answer
                            </button>
                            <button
                              className="riddle-list-item-action-button"
                              onClick={(e) => this.startTimer(riddle)}
                              disabled={
                                riddle.status === "Started" ||
                                riddle.status === "Done"
                              }
                            >
                              Start Quiz
                            </button>
                            <button
                              className="riddle-list-item-action-button"
                              onClick={(e) => this.stopTimer(riddle)}
                              disabled={riddle.status === "Done"}
                            >
                              End Quiz
                            </button>
                            <button
                              className="riddle-list-item-action-button"
                              onClick={(e) => this.shareLink(riddle)}
                            >
                              Share Link
                            </button>
                          </div>
                        </div>
                      </li>
                    ))
                  : "No Quizzes Added..."}
              </ul>
            </div>
          )
        ) : (
          <div className="create-new-riddles-container">
            <div className="riddles-header">
              <div className="riddles-header-left">
                <img
                  src={require("../../assets/icons/back.svg")}
                  alt="back"
                  className="riddles-back-button"
                  onClick={(e) => this.goBack(true)}
                />
                <h2>Create new Quiz</h2>
              </div>
            </div>
            <div className="riddle-form-container">
              <div className="riffle-question-container">
                <label className="form-label">Question</label>
                <textarea
                  name="riddle-question"
                  id="riddle-question"
                  rows="4"
                  placeholder="Enter your question here..."
                  className="riddle-question-textarea"
                  value={question}
                  onChange={(e) => this.setState({ question: e.target.value })}
                ></textarea>
              </div>
              <div className="riddle-options-container">
                <label className="form-label">Options</label>
                <div className="riddle-option-input-container">
                  <div className="riddle-option-input-item">
                    <input
                      type="text"
                      className="riddle-option-input"
                      placeholder="Add Options"
                      value={currentOption}
                      onChange={(e) =>
                        this.setState({ currentOption: e.target.value })
                      }
                    />
                    <button
                      type="button"
                      className="add-riddle-option-button"
                      onClick={this.addOptions}
                    >
                      <span className="add-icon">
                        <FontAwesomeIcon icon={faPlus} />
                      </span>
                      <span>Add</span>
                    </button>
                  </div>
                  <ul className="riddle-option-list-container">
                    {options.length
                      ? options.map((option, i) => (
                          <li className="riddle-option" key={i}>
                            <div className="riddle-option-item">
                              <span className="riddle-option-name">
                                {option}
                              </span>
                              <span
                                className="riddle-option-delete-button"
                                onClick={(e) => this.removeOption(option)}
                              >
                                <FontAwesomeIcon icon={faTrashAlt} />
                              </span>
                            </div>
                          </li>
                        ))
                      : "No option added..."}
                  </ul>
                </div>
              </div>
              <div className="riddle-answer-container">
                <label className="form-label">Answer</label>
                <input
                  type="text"
                  className="riddle-answer-input"
                  placeholder="Enter answer"
                  value={answer}
                  onChange={(e) => this.setState({ answer: e.target.value })}
                />
              </div>
              <div className="riddle-timer-container">
                <label className="form-label">Timer</label>
                <TimeField
                  value={timer}
                  className="riddle-timer-input"
                  onChange={(e) => this.setState({ timer: e.target.value })}
                />
              </div>
              <div className="riddle-submit-button">
                <button
                  type="button"
                  className="submit-button"
                  onClick={this.submitRiddle}
                >
                  <span className="button-text">Create Quiz</span>
                  <span className="button-icon">
                    <FontAwesomeIcon icon={faAngleRight} />
                  </span>
                </button>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(RiddlesScreen);
