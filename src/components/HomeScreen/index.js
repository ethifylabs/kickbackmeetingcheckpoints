import React from "react";
import "./home_screen.scss";
import { getAccount } from "../../services";
import { useHistory } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faAngleRight } from "@fortawesome/free-solid-svg-icons";

function HomeScreen(props) {
  let history = useHistory();
  const handleConnect = () => {
    return new Promise(async (resolve, reject) => {
      try {
        const address = await getAccount();
        console.log(address);
        props.setAddress(address);
        resolve();
      } catch (err) {
        reject(err);
      }
    });
  };

  const openApp = async () => {
    if (props.address) {
      history.push("/events");
    } else {
      try {
        await handleConnect();
        history.push("/events");
      } catch (err) {
        console.log(err);
      }
    }
  };

  return (
    <div className="home-screen">
      <div className="home-title">Welcome to CheckMeet</div>
      <div className="home-desc">
        Create checkpoint to keep track of your participants progress
      </div>
      <div className="open-app">
        <button className="open-app-button" onClick={openApp}>
          <span className="button-text">Open App</span>
          <span className="button-icon">
            <FontAwesomeIcon icon={faAngleRight} />
          </span>
        </button>
      </div>
    </div>
  );
}

export default HomeScreen;
