import React from "react";
import "./event_single_screen.scss";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus, faTrashAlt } from "@fortawesome/free-solid-svg-icons";
import makeBlockie from "ethereum-blockies-base64";
import { getEvent, deleteCheckpoint } from "../../services";

function EventSingleScreen({ address, setOpenModal, setModalConfig }) {
  let history = useHistory();
  const { eventId } = useParams();

  const [checkpoints, setCheckpoints] = React.useState([]);
  const [event, setEvent] = React.useState();

  React.useEffect(() => {
    handleGetEvent();
  }, []);

  const handleGetEvent = async () => {
    console.log(eventId);
    const resp = await getEvent(address, eventId);
    const event = resp.event;
    console.log(event);
    const resEvent = {
      ...event,
      stakedAmount: "1.50 ETH",
      // Number.parseFloat(
      //   Number(event.deposit) /
      //   Math.pow(10, Number.parseInt(event.tokenDecimals))
      // ).toFixed(2) +
      // " " +
      // event.tokenSymbol,
    };
    setEvent(resEvent);
    setCheckpoints(resEvent.checkpoints);
  };

  const openAddCheckpointModal = () => {
    console.log("Enter handle open modal");
    console.log(eventId);
    setModalConfig({
      type: "add-checkpoint",
      eventId,
      checkpoints,
      setCheckpoints,
    });
    setOpenModal(true);
  };

  const removeCheckpoint = async (checkpoint, checkId) => {
    await deleteCheckpoint(address, eventId, checkpoint.id);
    const newCheckpoints = checkpoints.filter((checkpoint, i) => checkId !== i);
    setCheckpoints(newCheckpoints);
  };

  const openCheckpoint = (checkpoint) => {
    history.push(`/events/${eventId}/checkpoint/${checkpoint.id}`);
  };

  const goBack = () => {
    history.push(`/events`);
  };

  return (
    <div className="event-single-screen">
      {event && (
        <div>
          {event.name && (
            <div className="event-single-header">
              <img
                src={require("../../assets/icons/back.svg")}
                alt="back"
                className="event-back-button"
                onClick={(e) => goBack()}
              />
              <div>
                <h2>{event.name}</h2>
                <span>ID: {event.id}</span>
              </div>
            </div>
          )}
          <div className="event-single-body">
            <div className="event-single-left">
              {event.headerImg && (
                <div className="event-image-container">
                  <img
                    src={event.headerImg}
                    alt="event"
                    className="event-image"
                  />
                </div>
              )}
              {event.roles.length !== 0 ? (
                <div className="event-organiser-container">
                  <div className="event-organiser-title">Admin</div>
                  <div className="event-organiser-list-container">
                    <ul className="event-organiser-list">
                      {event.roles.map((role, i) => (
                        <li className="event-organiser-items" key={i}>
                          <div className="event-organiser-item-image">
                            <img
                              src={makeBlockie(role.user.address)}
                              alt="metamask icon"
                              className="organiser-icon"
                            />
                          </div>
                          <div className="event-organiser-item-address">
                            {role.user.username}
                          </div>
                        </li>
                      ))}
                    </ul>
                  </div>
                </div>
              ) : null}
              <div className="event-staked-amount-container">
                <span className="event-staked-amount-title">
                  Total Staked:{" "}
                </span>
                <span className="event-staked-amount">
                  {event.stakedAmount}
                </span>
              </div>
              <div className="event-participants-container">
                <div className="event-participants-title">
                  {event.participants.length} Participants
                </div>
                <div className="event-participants-list-container">
                  <ul className="event-participants-list">
                    {event.participants.map((participant, j) => (
                      <li className="event-participants-items" key={j}>
                        <div className="event-participants-icons">
                          <img
                            src={makeBlockie(participant.user.address)}
                            alt="metamask icon"
                            className="participants-icon"
                          />
                        </div>
                        <div className="event-participants-address">
                          {participant.user.username}
                        </div>
                      </li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
            <div className="event-single-right">
              <div className="event-min-checkpoint-container">
                <span className="event-min-checkpoint-title">
                  Minimum Checkpoints needed to clear:{" "}
                </span>
                <span className="event-min-checkpoint-amount">
                  {event.minCheckpoint}
                </span>
              </div>
              <div className="event-checkpoint-list-container">
                <div className="event-checkpoint-list-title">
                  <span className="event-checkpoint-title-text">
                    Checkpoints
                  </span>
                  <button
                    className="create-checkpoint-button"
                    onClick={openAddCheckpointModal}
                  >
                    <span className="button-icon">
                      <FontAwesomeIcon icon={faPlus} />
                    </span>
                    <span className="button-text">Add Checkpoint</span>
                  </button>
                </div>
                <div className="event-checkpoint-list">
                  {checkpoints.length > 0
                    ? checkpoints.map((checkpoint, checkId) => (
                        <div
                          className="event-checkpoint-list-item"
                          key={checkId}
                        >
                          <div
                            className="event-checkpoint-details"
                            onClick={(e) => openCheckpoint(checkpoint)}
                          >
                            <div className="event-checkpoint-list-item-name">
                              {checkpoint.name}
                            </div>
                            <div className="event-checkpoint-list-item-status">
                              Status: {checkpoint.status}
                            </div>
                          </div>
                          <div
                            className="event-checkpoint-delete"
                            onClick={(e) =>
                              removeCheckpoint(checkpoint, checkId)
                            }
                          >
                            <FontAwesomeIcon icon={faTrashAlt} />
                          </div>
                        </div>
                      ))
                    : "No checkpoint added..."}
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default EventSingleScreen;
